import { createLogger, transports, format } from "winston";

export default createLogger({
    level: 'info',
    format: format.combine(
        format.timestamp(),
        format.json()
    ),
    transports: [
        // new transports.Console(), // console.log commented out for testing
        new transports.File({
            filename: 'logs/error.log',
            level: 'error'
        }),
        // using this & http causes duplicate logs:
        // new transports.File({
        //     filename: 'logs/combined.log'
        // }),
        new transports.File({
            filename: 'logs/combined.log',
            level: 'http'
        })
    ]
});