import express from 'express';
import Counter from './counter.mjs';
import logger from './logger.mjs';
import endpoint_mw from './endpoint.mw.mjs';

const app = express();
const counter = new Counter

// Middleware
app.use(endpoint_mw);

// Endpoints
app.get('', (req, res) => res.send("Welcome!"));

// GET /counter-increase
app.get('/counter-increase', (req, res) => {
    // logger.http("[ENDPOINT] GET '/counter-increase'");
    counter.increase();
    res.send(`${counter.read()}`);
});

//GET /counter-read
app.get('/counter-read', (req, res) => {
    // logger.http("[ENDPOINT] GET '/counter-read'");
    res.send(`${counter.read()}`);
});

// GET /counter-zero
app.get('/counter-zero', (req, res) => {
    // logger.http("[ENDPOINT] GET '/counter-zero'");
    counter.zero();
    res.send(`${counter.read()}`);
});

app.all('*', (req, res) => {
    res.status(404).send("Resource not found.");
    // logger.http(`[ENDPOINT] ${req.method} '${req.url}'`);
});

export default app;

