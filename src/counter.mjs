import logger from './logger.mjs';

class Counter {
    constructor(initialValue = 0) {
        this.count = initialValue;
    }

    increase() {
        this.count++
        logger.info("[COUNTER] increase " + this.count);
    }

    zero() {
        this.count = 0;
        logger.info("[COUNTER] zeroed " + this.count);
    }

    read() {
        logger.info("[COUNTER] read " + this.count);
        return this.count;
    }
}

export default Counter;