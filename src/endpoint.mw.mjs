import logger from './logger.mjs';

/** @type {import('express').RequestHandler;} */
const endpoint_mw = (req, res, next) => {
    // console.log("Request: " + req.method + " " + req.url);
    logger.http(`[ENDPOINT] ${req.method} '${req.url}'`);
    next();
};

export default endpoint_mw